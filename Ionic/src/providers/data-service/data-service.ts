import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the DataServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DataServiceProvider
{
  private static username: string;
  private static tasks: any = [];
  private static categories: any = [];
  private static loginKey: string;
  public static filter;

  apiUrl: string = "http://localhost:58358/api";

  constructor(private http: HttpClient)
  {

  }

  public login(username: string, password: string)
  {
    console.log("Attemping to log in...");

    var response;

    return new Promise((resolve, reject) =>
    {
      this.http.get(this.apiUrl + "/login?username=" + username + "&password=" + password).subscribe
        (
        data =>
        {
          resolve(data)
          console.log("Successfully logged in.");
        },
        error =>
        {
          reject(error)
          console.log("Failed to log in.");
        }
        );
    })
  }

  public signup(username: string, password: string)
  {
    console.log("Attemping to sign up...");

    var response;

    return new Promise((resolve, reject) =>
    {
      this.http.get(this.apiUrl + "/signup?username=" + username + "&password=" + password).subscribe
        (
        data =>
        {
          resolve(data)
          console.log("Successfully signed up.");
        },
        error =>
        {
          reject(error)
          console.log("Failed to sign up.");
        }
        );
    })
  }

  //Get functions that provide user authentication
  getUserTasks()
  {
    return new Promise((resolve, reject) =>
    {
      this.http.get(this.apiUrl + "/Tasks" + DataServiceProvider.KeyUsernameAuth()).subscribe
      (
        (data) => { resolve(data) },
        (error) => { reject(error) }
      )
    })
  }

  getUserTasksOrdered(order)
  {
    return new Promise((resolve, reject) =>
    {
      this.http.get(this.apiUrl + "/Tasks" + DataServiceProvider.KeyUsernameAuth() +"&order=" + order).subscribe
        (
        (data) => { resolve(data) },
        (error) => { reject(error) }
        )
    })
  }

  getUserCategories()
  {
    return new Promise((resolve, reject) =>
    {
      this.http.get(this.apiUrl + "/Categories" + DataServiceProvider.KeyUsernameAuth()).subscribe
        (
        (data) =>
        {
          resolve(data)
        },
        (error) => { reject(error) }
        )
    })
  }

  getCategories()
  {
    return new Promise((resolve, reject) =>
    {
      this.http.get(this.apiUrl + "/Categories?=key" + DataServiceProvider.getLoginKey()).subscribe
      (
        (data) => { resolve(data) },
        (error) => { reject(error) }
      )
    })
  }

  get(entity : String)
  {
    return new Promise((resolve, reject) =>
    {
      this.http.get(this.apiUrl + "/" + entity + "?=key" + DataServiceProvider.getLoginKey()).subscribe
        (
        (data) => { resolve(data) },
        (error) => { reject(error) }
        )
    })
  }

  addUserTask(task)
  {
    return new Promise((resolve, reject) =>
    {
      this.http.post(this.apiUrl + '/Tasks' + DataServiceProvider.KeyUsernameAuth(), task)
        .subscribe(res =>
        {
          resolve(res);
        }, (err) =>
        {
          reject(err);
        });
    });
  }

  addUserCategory(category)
  {
    return new Promise((resolve, reject) =>
    {
      this.http.post(this.apiUrl + '/Categories' + DataServiceProvider.KeyUsernameAuth(), category)
        .subscribe(res =>
        {
          resolve(res);
        }, (err) =>
        {
          reject(err);
        });
    });
  }

  deleteUserTask(task)
  {
    return new Promise((resolve, reject) =>
    {
      console.log(task);
      this.http.delete(this.apiUrl + '/Tasks' + DataServiceProvider.KeyUsernameAuth() + "&taskid=" + task.taskid)
        .subscribe(res =>
        {
          resolve(res);
        }, (err) =>
        {
          reject(err);
        });
    });
  }

  editUserTask(task)
  {
    return new Promise((resolve, reject) =>
    {
      console.log(task);
      this.http.put(this.apiUrl + '/Tasks' + DataServiceProvider.KeyUsernameAuth() + "&id=" + task.taskid, task)
        .subscribe(res =>
        {
          resolve(res);
        }, (err) =>
        {
          reject(err);
        });
    });
  }

  private static getLoginKey()
  {
    return DataServiceProvider.loginKey;
  }

  public static setLoginKey(loginKey: string)
  {
    DataServiceProvider.loginKey = loginKey;
  }

  public static getUsername()
  {
    return DataServiceProvider.username;
  }

  public static setUsername(username: string)
  {
    DataServiceProvider.username = username;
  }

  private static getTasks()
  {
    return DataServiceProvider.tasks;
  }

  public static setTasks(tasks: any=[])
  {
    DataServiceProvider.tasks = tasks;
  }

  private static getCategories()
  {
    return DataServiceProvider.categories;
  }

  public static setCategories(categories: any=[])
  {
    DataServiceProvider.categories = categories;
  }

  public static KeyUsernameAuth()
  {
    return "?key=" + DataServiceProvider.getLoginKey() + "&username=" + DataServiceProvider.getUsername();
  }

}
