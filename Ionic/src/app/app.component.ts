import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';

import { StartUpPage } from '../pages/start-up/start-up';
import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { NewTaskPage } from '../pages/new-task/new-task';
import { CategoriesPopoverPage } from '../pages/categories-popover/categories-popover';
import { CreateCategoryPopoverPage } from '../pages/create-category-popover/create-category-popover';
import { TaskOptionsPopoverPage } from '../pages/task-options-popover/task-options-popover';
import { EditTaskPage } from '../pages/edit-task/edit-task';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = StartUpPage;

  constructor(platform: Platform) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
    });
  }
}

