import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { StartUpPage } from '../pages/start-up/start-up';
import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { NewTaskPage } from '../pages/new-task/new-task';
import { EditTaskPage } from '../pages/edit-task/edit-task';

import { CategoriesPopoverPage } from '../pages/categories-popover/categories-popover';
import { CreateCategoryPopoverPage } from '../pages/create-category-popover/create-category-popover';
import { TaskOptionsPopoverPage } from '../pages/task-options-popover/task-options-popover';
import { TaskFilterPopoverPage } from '../pages/task-filter-popover/task-filter-popover';

import { DataServiceProvider } from '../providers/data-service/data-service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    MyApp,
    StartUpPage,
    LoginPage,
    HomePage,
    NewTaskPage,
    EditTaskPage,
    CategoriesPopoverPage,
    CreateCategoryPopoverPage,
    TaskOptionsPopoverPage,
    TaskFilterPopoverPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents:
  [
    MyApp,
    StartUpPage,
    LoginPage,
    HomePage,
    NewTaskPage,
    EditTaskPage,
    CategoriesPopoverPage,
    CreateCategoryPopoverPage,
    TaskOptionsPopoverPage,
    TaskFilterPopoverPage
  ],
  providers:
  [
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DataServiceProvider
  ]
})
export class AppModule {}
