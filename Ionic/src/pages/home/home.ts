import { Component } from '@angular/core';
import { NavController, ToastController, PopoverController } from 'ionic-angular';
import { Debug } from '../../Debug';
import { Content } from 'ionic-angular';
import { ViewChild } from '@angular/core';

import { NewTaskPage } from '../new-task/new-task';
import { TaskOptionsPopoverPage } from '../task-options-popover/task-options-popover';

import { DataServiceProvider } from '../../providers/data-service/data-service';
import { DateTime } from 'ionic-angular/components/datetime/datetime';
import { EditTaskPage } from '../edit-task/edit-task';
import { TaskFilterPopoverPage } from '../task-filter-popover/task-filter-popover';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage
{
  @ViewChild(Content) content: Content;

  sortUpIcon: String = "md-arrow-round-up";
  sortDownIcon: String = "md-arrow-round-down";
  sortIcon: String;
  tasks: any = [];

  /* This page is created by : LoginPage, HomePage, NewTask
   * This page creates pages : TaskOptionsPopoverPage, TaskFilterPopoverPage, NewTaskPage
   * Use : The reason this page is created by itself and by NewTask is to refresh this page
   *       so that the constructor is called. This is a bad way of refreshing the elements,
   *       a method that exists inside of this script should be called when a refresh should
   *       occur.
   *       This page is also responsible for various actions: Filtering, sorting, multiple toasts and popovers
   */
  constructor(public navCtrl: NavController, private dsProvider: DataServiceProvider,
              private popoverCtrl: PopoverController, private toastCtrl: ToastController)
  {
    //get categories and users and store them in a static variable(bad practice)
    this.dsProvider.getUserCategories().then
    (
      (data) =>
      {
        DataServiceProvider.setCategories(data);
      },
      (error) => console.log(error)
    );

    //When getting tasks, get them in ascending order, if there are no tasks, crease one stating that there are no tasks.
    this.dsProvider.getUserTasksOrdered(1).then
    (
      (data) =>
      {
        this.sortIcon = this.sortUpIcon;
        this.tasks = this.filter(data);
        this.setTimeDifference();
        if (this.tasks.length < 1)
        {
          if (!DataServiceProvider.filter)
          {
            this.tasks = [{ name: "You have no tasks!", description: "Click \'New Task\' to create one!" }];
          }
          else
          {
            this.tasks = [{ name: "Nadda!", description: "There are no tasks that fit this search!" }];
          }
        }
      },
      (error) => console.log(error)
    );
  }

  //called when tasks is loaded. adds an extra variable to each task which equals how much time the task has left.
  private setTimeDifference()
  {
    //After loading tasks
    for (var ii = 0; ii < this.tasks.length; ii++)
    {
      if (this.tasks[ii].duedate != null)
      {
        var dueDate = new Date(this.tasks[ii].duedate);
        var timeDiff = Math.abs(dueDate.getTime() - Date.now());
        var diffDays = Math.floor(timeDiff / (1000 * 3600 * 24 * 360));

        this.tasks[ii].timeleft = diffDays + "yr left";
        if (diffDays < 1) //if less than 1 year
        {
          var diffDays = Math.floor(timeDiff / (1000 * 3600 * 24 * 30));
          this.tasks[ii].timeleft = diffDays + "mth left";

          if (diffDays < 1) //if less than 1 month
          {
            var diffDays = Math.floor(timeDiff / (1000 * 3600 * 24));
            this.tasks[ii].timeleft = diffDays + "d left";

            if (diffDays < 1) //if less than 1 day
            {
              var diffDays = Math.floor(timeDiff / (1000 * 3600));
              this.tasks[ii].timeleft = diffDays + "hr left";

              if (diffDays < 1) //if less than 1 hour
              {
                var diffDays = Math.floor(timeDiff / (1000));
                this.tasks[ii].timeleft = diffDays + "min left";

                if (diffDays < 1) //if less than 1 minute
                {
                  this.tasks[ii].timeleft = "Few Seconds left";
                  if (timeDiff < 0) //if less than 1 day
                  {
                    this.tasks[ii].timeleft = diffDays + "EXPIRED";
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  ionViewDidLoad()
  {
    Debug.SuccessfullyLoadedPage(HomePage.name);
  }

  /*opens the TaskOptionsPopoverPage and expects a string response which it then uses to decide whether to delete
   * a task,edit a task or do nothing*/
  openTaskOptions(task)
  {
    let popover = this.popoverCtrl.create(TaskOptionsPopoverPage);
    popover.onDidDismiss
      (
      data =>
      {
        if (data == "delete")
        {
          this.dsProvider.deleteUserTask(task).then
          (
            (data) =>
            {
              let toast = this.toastCtrl.create
              ({
                position: 'top',
                message: 'Deleted task : ' + task.name,
                duration: 2000
              });
              toast.present();
              this.navCtrl.setRoot(HomePage);
            },
            (error) => console.log(error)
          );
        }
        else if (data == "edit")
        {
          this.navCtrl.push(EditTaskPage, { param1: task });
        }
      }
      );
    popover.present();
  }

  /*Is used for determining the current sorting state and for calling the Provider that will call the api for
   * sorted tasks*/
  sortTasks()
  {
    var order: number;
    if (this.sortIcon == this.sortUpIcon)
    {
      order = -1;
      this.sortIcon = this.sortDownIcon;
    }
    else
    {
      order = 1;
      this.sortIcon = this.sortUpIcon;
    }

    this.dsProvider.getUserTasksOrdered(order).then
    (
      data =>
      {
        this.tasks = this.filter(data),
        this.setTimeDifference()
      }
    )

  }

  /*This function is inaptly named and only opens the FilterPopover and refreshes the page when done.*/
  filterTasks()
  {
    let popover = this.popoverCtrl.create(TaskFilterPopoverPage);
    popover.onDidDismiss
    (
      data =>
      {
        this.navCtrl.setRoot(HomePage);
      }
    );
    popover.present();
  }

  /*This function contains the logic for the filtering, it is called by many functions, including the constructor*/
  filter(tasks)
  {
    var filter: any = DataServiceProvider.filter;

    console.log(filter);
    if (filter)
    {
      //for each task
      for (var ii = 0; ii < tasks.length; ii++)
      {
        var splice = false;
        //only remove from list if categoryname is not same as filter unless filter catname is "any" then never remove based on cat name. Only remove if date filters arn't null and if cat times are outside of filter times
        if (tasks[ii].categoryname != filter.categoryname && filter.categoryname != "all")         
        {
          splice = true;
        }
        else if ((filter.startdate && filter.enddate))
        {
          console.log("(" + Date.parse(tasks[ii].duedate) + " < " + Date.parse(filter.startdate) + "[" + (Date.parse(tasks[ii].duedate) < Date.parse(filter.startdate)) + " ] || (" + Date.parse(tasks[ii].duedate) + " > " + Date.parse(filter.enddate) + "[" + (Date.parse(tasks[ii].duedate) > Date.parse(filter.enddate)) + "])");
          if (tasks[ii].duedate == null)
          {
            splice = true;
          }
          if ((Date.parse(tasks[ii].duedate) < Date.parse(filter.startdate)) || (Date.parse(tasks[ii].duedate) > Date.parse(filter.enddate)))
          {
            splice = true;
          }
        }

        if (splice)
        {
          console.log("spliced : " + ii);
          tasks.splice(ii, 1);
          ii--;
        }
      }
    }

    return tasks;
  }

  loadAddPage()
  {
    Debug.PushingPage(NewTaskPage);
    this.navCtrl.push(NewTaskPage);
  }
}
