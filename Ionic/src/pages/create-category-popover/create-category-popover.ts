import { Component } from '@angular/core';
import { IonicPage, ViewController, NavController, NavParams } from 'ionic-angular';
import { DataServiceProvider } from '../../providers/data-service/data-service';

/**
 * Generated class for the CreateCategoryPopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-category-popover',
  templateUrl: 'create-category-popover.html',
})
export class CreateCategoryPopoverPage {

  info: String;
  infoColour: String;
  category: any = {};

  constructor(public navCtrl: NavController, private viewCtrl: ViewController, public navParams: NavParams, private dsProvider: DataServiceProvider)
  {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateCategoryPopoverPage');
  }

  createCategory()
  {
    if (this.category.name)
    {
      this.category.username = DataServiceProvider.getUsername();
      this.dsProvider.addUserCategory(this.category).then
        (
        (data) =>
        {
          this.viewCtrl.dismiss(data);
        },
        (error) =>
        {
          this.infoColour = "red";
          this.info = "Unable to create this category!";
        }
        );
    }
    else
    {
      this.infoColour = "red";
      this.info = "Please enter a name for your category.";
    }
  }

}
