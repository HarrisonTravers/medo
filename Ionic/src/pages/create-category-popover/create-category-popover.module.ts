import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateCategoryPopoverPage } from './create-category-popover';

@NgModule({
  declarations: [
    CreateCategoryPopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateCategoryPopoverPage),
  ],
})
export class CreateCategoryPopoverPageModule {}
