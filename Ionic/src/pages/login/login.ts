import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController} from 'ionic-angular';
import { ToastController } from 'ionic-angular';

import { HomePage } from '../home/home';

import { Debug } from '../../Debug';
import { DataServiceProvider } from '../../providers/data-service/data-service';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public user: any = {};
  public loginInformation = "";
  private infoColour = "red";

  /*This page can be created by : StartUpPage
   * This page can create : HomePage
   * User : This page is simply a login screen, with a bare-minimum sign-up feature.
   *        if anything goes wrong, for example, a user enters the wrong password.
   *        The user will be notified using a text label, as is standard.
  */
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private dsProvider: DataServiceProvider, private toastCtrl: ToastController,
              private loadingCtrl: LoadingController)
  {

  }

  ionViewDidLoad()
  {
    Debug.SuccessfullyLoadedPage(LoginPage);
  }

  private login()
  {
    if (this.user.username && this.user.password)
    {
      this.presentLoadingDefault();
      this.dsProvider.login(this.user.username, this.user.password).then
      (
        (data) =>
        {
          this.infoColour = "green";
          this.loginInformation = "Success, now logging you in.";

          DataServiceProvider.setLoginKey(data.toString());
          DataServiceProvider.setUsername(this.user.username);

          
          let toast = this.toastCtrl.create({
            position: 'top',
            message: 'Welcome ' + this.user.username + '!',
            duration: 2000
          });
          toast.present();
          

          this.navCtrl.setRoot(HomePage);
        },
        (error) =>
        {
          this.infoColour = "red";
          console.log(error.error.Message);

          var loginStatus = error.error.Message;
          
          if (loginStatus == "BadPassword")
          {
            //if username exists but password is wrong
            this.loginInformation = "The password you entered is incorrect, please try again."
          }
          else if (loginStatus == "BadUsername")
          {
            //if username doesn't exist
            this.loginInformation = "The username you entered doesn't exist, please try again."
          }
          else
          {
            //when the status returned is out of expected scope
            this.loginInformation = "Something went wrong, please try again in a few minutes."
          }
        }
      );     
    }
  }

  private signup()
  {
    if (this.user.username && this.user.password.toString())
    {
      this.dsProvider.signup(this.user.username, this.user.password).then
        (
        (data) =>
        {
          this.infoColour = "green";
          this.loginInformation = "Account created! now logging you in.";

          this.login();
        },
        (error) =>
        {
          this.infoColour = "red";

          console.log(error.error.Message);

          var loginStatus = error.error.Message;

          if (loginStatus == "BadUsername")
          {
            //if username doesn't exist
            this.loginInformation = "The username you entered already exists, please try again."
          }
          else
          {
            //when the status returned is out of expected scope
            this.loginInformation = "Something went wrong, please try again in a few minutes."
          }
        }
        );
    }
    else
    {
      this.infoColour = "red";
      this.loginInformation = "Enter your desired username and password, then click signup!";
    }
  }

  presentLoadingDefault()
  {
    let loading = this.loadingCtrl.create({
      content: 'Logging you in...'
    });

    loading.present();

    setTimeout(() =>
    {
      loading.dismiss();
    }, 500);
  }


}
