import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TaskFilterPopoverPage } from './task-filter-popover';

@NgModule({
  declarations: [
    TaskFilterPopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(TaskFilterPopoverPage),
  ],
})
export class TaskFilterPopoverPageModule {}
