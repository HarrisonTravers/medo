import { Component } from '@angular/core';
import { IonicPage, PopoverController, ViewController, NavController, NavParams } from 'ionic-angular';
import { DataServiceProvider } from '../../providers/data-service/data-service';
import { CategoriesPopoverPage } from '../categories-popover/categories-popover';

/**
 * Generated class for the TaskFilterPopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-task-filter-popover',
  templateUrl: 'task-filter-popover.html',
})
export class TaskFilterPopoverPage {

  currentDate: String = new Date().getDate.toString();
  currentTime: String = new Date().getTime().toString();
  minDate: String = new Date().toISOString();

  filter: any = {};

  constructor(public navCtrl: NavController, private popoverCtrl: PopoverController, private viewCtrl: ViewController,
              public navParams: NavParams)
  {
    this.filter.categoryname = "all";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TaskFilterPopoverPage');
  }

  filterTasks()
  {
    DataServiceProvider.filter = this.filter;
    this.viewCtrl.dismiss();
  }

  removeFilter()
  {
    DataServiceProvider.filter = null;
    this.viewCtrl.dismiss();
  }

  openCategories(myEvent)
  {
    let popover = this.popoverCtrl.create(CategoriesPopoverPage);
    popover.onDidDismiss
      (
      data =>
      {
        if (data)
        {
          this.filter.categoryname = data.name
        }
      }
      );
    popover.present();
  }

}
