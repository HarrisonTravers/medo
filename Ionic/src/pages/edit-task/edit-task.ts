import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PopoverController } from 'ionic-angular';
import { DataServiceProvider } from '../../providers/data-service/data-service';

import { CategoriesPopoverPage } from '../categories-popover/categories-popover';
import { HomePage } from '../home/home';

/**
 * Generated class for the NewTaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-task',
  templateUrl: 'edit-task.html',
})
export class EditTaskPage
{

  info: String;
  infoColour: String;

  currentDate: String = new Date().getDate.toString();
  currentTime: String = new Date().getTime().toString();
  minDate: String = new Date().toISOString();
  categories: any = [];

  task: any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, private popoverCtrl: PopoverController,
    private dsProvider: DataServiceProvider)
  {
    this.task = this.navParams.get('param1');

    this.task.username = DataServiceProvider.getUsername();
  }

  ionViewDidLoad()
  {
    console.log('ionViewDidLoad EditTaskPage');
  }

  openCategories(myEvent)
  {
    let popover = this.popoverCtrl.create(CategoriesPopoverPage);
    popover.onDidDismiss
    (
      data =>
      {
        if (data)
        {
          this.task.categoryname = data.name,
            this.task.categoryid = data.categoryid
        }
      }
    );
    popover.present();
  }

  saveChanges()
  {
    if (this.task.name && this.task.categoryname)
    {
      this.dsProvider.editUserTask(this.task).then
      (
        (data) =>
        {
          this.navCtrl.setRoot(HomePage);
        },
        (error) =>
        {
          this.infoColour = "red";
          this.info = "Something went wrong, please try again later.";
        }
      );
    }
    else
    {
      this.infoColour = "red";
      this.info = "Please ensure that the task's name and category is not empty.";
    }
  }

}
