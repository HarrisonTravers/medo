import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { DataServiceProvider } from '../../providers/data-service/data-service';

import { Debug } from '../../Debug';

@IonicPage()
@Component
  (
    {
      selector: 'page-start-up',
      templateUrl: 'start-up.html',
    }
  )

  /*This page is the first to run at startup, simply loads a splash and has a button that goes to the login page*/
export class StartUpPage {

  constructor(public navCtrl: NavController, public navParams: NavParams)
  {

  }

  ionViewDidLoad()
  {
    Debug.SuccessfullyLoadedPage(StartUpPage.name);
  }

  pushLoginPage()
  {
    Debug.PushingPage(LoginPage);
    this.navCtrl.push(LoginPage);
  }
}
