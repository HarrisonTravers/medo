import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TaskOptionsPopoverPage } from './task-options-popover';

@NgModule({
  declarations: [
    TaskOptionsPopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(TaskOptionsPopoverPage),
  ],
})
export class TaskOptionsPopoverPageModule {}
