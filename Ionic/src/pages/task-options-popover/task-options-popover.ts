import { Component } from '@angular/core';
import { IonicPage, ViewController, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the TaskOptionsPopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-task-options-popover',
  templateUrl: 'task-options-popover.html',
})
export class TaskOptionsPopoverPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TaskOptionsPopoverPage');
  }

  editTask()
  {
    this.viewCtrl.dismiss("edit");
  }

  deleteTask()
  {
    this.viewCtrl.dismiss("delete");
  }

}
