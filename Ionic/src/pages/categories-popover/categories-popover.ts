import { Component } from '@angular/core';
import { IonicPage, PopoverController, ViewController, NavController, NavParams } from 'ionic-angular';
import { DataServiceProvider } from '../../providers/data-service/data-service';
import { PopoverCmp } from 'ionic-angular/components/popover/popover-component';

import { CreateCategoryPopoverPage } from '../create-category-popover/create-category-popover';

/**
 * Generated class for the CategoriesPopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-categories-popover',
  templateUrl: 'categories-popover.html',
})
export class CategoriesPopoverPage {

  categories: any = [];
  selectedCategory: any;

  constructor(public navCtrl: NavController, public viewCtrl: ViewController,
              public navParams: NavParams, public dsProvider: DataServiceProvider,
              private popoverCtrl : PopoverController)
  {
    this.dsProvider.getUserCategories().then
    (
      (data) =>
      {
        this.categories = data;
      },
      (error) =>
      {
        
      }
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoriesPopoverPage');
  }

  selectCategory(category)
  {
    this.viewCtrl.dismiss(category);
  }

  createCategory()
  {
    let popover = this.popoverCtrl.create(CreateCategoryPopoverPage);
    popover.onDidDismiss
      (
      data =>
      {
        this.selectCategory(data)
      }
      );
    popover.present();
  }

}
