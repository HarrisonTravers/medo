import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategoriesPopoverPage } from './categories-popover';

@NgModule({
  declarations: [
    CategoriesPopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(CategoriesPopoverPage),
  ],
})
export class CategoriesPopoverPageModule {}
