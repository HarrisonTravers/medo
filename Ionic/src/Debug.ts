export class Debug {
  public static PushingPage(page)
  {
    console.log("Pushing Page : " + page.name + ".");
  }

  public static PoppingPage(page)
  {
    console.log("Popping Page : " + page.name + ".");
  }

  public static RootingPage(page)
  {
    console.log("Setting Page : " + page.name + " to root.");
  }

  public static SuccessfullyLoadedPage(page)
  {
    console.log("Successfully Loaded Page : " + page.name + ".")
  }
}
