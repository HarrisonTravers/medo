﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Utilities
{
    public class LoginSessionUtility
    {
        private static Random random = new Random();

        public static string CreateKey(int keyLength)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, keyLength)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}