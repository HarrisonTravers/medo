﻿using System;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using DatabaseAccessor;

namespace WebApp.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TasksController : ApiController
    {
        private MeDoEntities db = new MeDoEntities();

        // GET: api/Tasks
        public IQueryable<Task> GetTasks()
        {
            return db.Tasks;
        }

        // GET: api/Tasks/5
        [ResponseType(typeof(Task))]
        public IHttpActionResult GetTask(int id)
        {
            Task task = db.Tasks.Find(id);
            if (task == null)
            {
                return NotFound();
            }

            return Ok(task);
        }

        // GET: api/Categories
        [Route("api/Tasks")]
        [HttpGet]
        public IHttpActionResult GetUserTasks(string key, string username)
        {
            IHttpActionResult result;
            User user = db.Users.Find(username);

            if (user != null)
            {
                if (user.sessionkey.Equals(key))
                {
                    Task[] userTasks= db.Tasks.Where(entity => entity.username == username).ToArray();                    

                    result = Ok(userTasks);
                }
                else
                {
                    result = BadRequest("BadKey");
                }
            }
            else
            {
                result = BadRequest("BadUsername");
            }

            return result;
        }

        // GET: api/Categories
        [Route("api/Tasks")]
        [HttpGet]
        public IHttpActionResult GetUserTasksOrdered(string key, string username, int order)
        {
            IHttpActionResult result;
            User user = db.Users.Find(username);

            if (user != null)
            {
                if (user.sessionkey.Equals(key))
                {
                    Task[] userTasks = db.Tasks.Where(entity => entity.username == username).ToArray();
                    DateTime currentTime = DateTime.Now;

                    if(order < 0)
                    {
                        userTasks = userTasks.OrderBy(task => task.duedate).ToArray();
                    }
                    else
                    {
                        userTasks = userTasks.OrderByDescending(task => task.duedate).ToArray();
                    }

                    result = Ok(userTasks);
                }
                else
                {
                    result = BadRequest("BadKey");
                }
            }
            else
            {
                result = BadRequest("BadUsername");
            }

            return result;
        }


        [ResponseType(typeof(void))]
        [Route("api/Tasks")]
        [HttpPut]
        public IHttpActionResult PutUserTask(string key, string username, int id, Task task)
        {
            IHttpActionResult result;

            if (db.Users.Find(username).sessionkey.Equals(key))
            {
                if (ModelState.IsValid)
                {
                    db.Entry(task).State = EntityState.Modified;

                    try
                    {
                        db.SaveChanges();
                        result = StatusCode(HttpStatusCode.NoContent);
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!TaskExists(id))
                        {
                            result = NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                }
                else
                {
                    result = BadRequest(ModelState);
                }
            }
            else
            {
                result = BadRequest("BadKey");
            }

            return result;
        }

        // POST: api/Tasks
        [ResponseType(typeof(Task))]
        [Route("api/Tasks")]
        [HttpPost]
        public IHttpActionResult PostUserTask(string key, string username, Task task)
        {
            IHttpActionResult result;
            User user = db.Users.Find(username);

            if (user != null)
            {
                if (user.sessionkey.Equals(key))
                {
                    if (ModelState.IsValid)
                    {
                        task.createdat = DateTime.Now;
                        task.updatedat = DateTime.Now;
                        db.Tasks.Add(task);

                        try
                        {
                            db.SaveChanges();
                            result = Ok(task);
                        }
                        catch(Exception)
                        {
                            throw;
                        }
                    }
                    else
                    {
                        result = BadRequest(ModelState);
                    }
                }
                else
                {
                    result = BadRequest("BadKey");
                }
            }
            else
            {
                result = BadRequest("BadUsername");
            }

            return result;
        }

        // POST: api/Tasks
        [ResponseType(typeof(int))]
        [Route("api/Tasks")]
        [HttpDelete]
        public IHttpActionResult DeleteUserTask(string key, string username, int taskid)
        {
            IHttpActionResult result;
            User user = db.Users.Find(username);

            if (user != null)
            {
                if (user.sessionkey.Equals(key))
                {
                    Task newTask= db.Tasks.Find(taskid);
                    db.Tasks.Remove(newTask);
                    db.SaveChanges();

                    if (db.Tasks.Find(taskid) == null)
                    {
                        result = Ok(taskid);
                    }
                    else
                    {
                        result = NotFound();
                    }
                }
                else
                {
                    result = BadRequest("BadKey");
                }
            }
            else
            {
                result = BadRequest("BadUsername");
            }

            return result;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TaskExists(int id)
        {
            return db.Tasks.Count(e => e.taskid == id) > 0;
        }
    }
}