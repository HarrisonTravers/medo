﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using DatabaseAccessor;
using WebApp.Utilities;

namespace WebApp.Controllers
{
    [EnableCors(origins:"*", headers:"*", methods:"*")]
    public class UsersController : ApiController
    {
        private MeDoEntities db = new MeDoEntities();

        // GET: api/Users
        public IQueryable<User> GetUsers()
        {
            return db.Users;
        }

        // GET: api/Users/5
        [ResponseType(typeof(User))]
        public IHttpActionResult GetUser(string id)
        {
            User user = db.Users.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        // PUT: api/Users/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUser(string id, User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.username)
            {
                return BadRequest();
            }

            db.Entry(user).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Users
        [ResponseType(typeof(User))]
        public IHttpActionResult PostUser(User user)
        {
            Console.WriteLine("post received");
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Users.Add(user);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (UserExists(user.username))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = user.username }, user);
        }

        // DELETE: api/Users/5
        [ResponseType(typeof(User))]
        public IHttpActionResult DeleteUser(string id)
        {
            List<User> users = db.Users.ToList();
            User user = db.Users.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            db.Users.Remove(user);
            db.SaveChanges();

            return Ok(user);
        }


        [Route("api/login")]
        [HttpGet]
        [ResponseType(typeof(string))]
        public IHttpActionResult Login(string username, string password)
        {
            User user = db.Users.Find(username);
            IHttpActionResult result = BadRequest();

            if (user != null)
            {
                if(user.password.Equals(password))
                {
                    string key = LoginSessionUtility.CreateKey(16);
                    user.sessionkey = key;
                    db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();
                    result = Ok(key);
                }
                else
                {
                    result = BadRequest("BadPassword");
                }
            }
            else
            {
                result = BadRequest("BadUsername");
            }

            return result;

        }

        [Route("api/signup")]
        [HttpGet]
        [ResponseType(typeof(string))]
        public IHttpActionResult Signup(string username, string password)
        {
            User user = db.Users.Find(username);
            IHttpActionResult result = BadRequest();

            User newUser = new User
            {
                username = username,
                password = password
            };

            db.Users.Add(newUser);
            db.SaveChanges();

            Category newCategory = new Category
            {
                name = "Misc",
                username = newUser.username,
                createdat = DateTime.Now,
                updatedat = DateTime.Now
            };

            db.Categories.Add(newCategory);
            db.SaveChanges();

            Task newTask = new Task()
            {
                username = newUser.username,
                name = "Make a task",
                description = "To create a task, click \"New Task\" at the bottom of the screen!\nWhen you're done, tap on me and delete me.",
                categoryid = newCategory.categoryid,
                categoryname = newCategory.name,
                createdat = DateTime.Now,
                updatedat = DateTime.Now
            };
            db.Tasks.Add(newTask);

            try
            {
                db.SaveChanges();
                result = Ok("User successfully created");
            }
            catch (DbUpdateException)
            {
                if (UserExists(user.username))
                {
                    result = BadRequest("BadUsername");
                }
                else
                {
                    throw;
                }
            }

            return result;
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(string id)
        {
            return db.Users.Count(e => e.username == id) > 0;
        }
    }
}