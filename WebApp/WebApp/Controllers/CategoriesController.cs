﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using DatabaseAccessor;

namespace WebApp.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CategoriesController : ApiController
    {
        private MeDoEntities db = new MeDoEntities();

        // GET: api/Categories
        public IQueryable<Category> GetCategories()
        {
            return db.Categories;
        }

        // GET: api/Categories
        [Route("api/Categories")]
        [HttpGet]
        public IHttpActionResult GetCategories(string key, string username)
        {
            IHttpActionResult result;

            if (db.Users.Find(username).sessionkey.Equals(key))
            {
                result = Ok(db.Categories.Where(entity => entity.username == username));
            }
            else
            {
                result = BadRequest("BadKey");
            }

            return result;
        }

        // GET: api/Categories/5
        [ResponseType(typeof(Category))]
        public IHttpActionResult GetCategory(string key, string username, int id)
        {
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return NotFound();
            }

            return Ok(category);
        }

        // PUT: api/Categories/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCategory(string key, string username, int id, Category category)
        {
            IHttpActionResult result;

            if (db.Users.Find(username).sessionkey.Equals(key))
            {
                if (!ModelState.IsValid)
                {
                    db.Entry(category).State = EntityState.Modified;

                    try
                    {
                        db.SaveChanges();
                        result = StatusCode(HttpStatusCode.NoContent);
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!CategoryExists(id))
                        {
                            result = NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                }    
                else
                {
                    result = BadRequest(ModelState);
                }                
            }
            else
            {
                result = BadRequest("BadKey");
            }

            return result;            
        }

        // POST: api/Categories
        [ResponseType(typeof(Category))]
        [Route("api/Categories")]
        [HttpPost]
        public IHttpActionResult PostUserCategory(string key, string username, Category category)
        {
            IHttpActionResult result;
            User user = db.Users.Find(username);

            if (user != null)
            {
                if (user.sessionkey.Equals(key))
                {
                    if (ModelState.IsValid)
                    {
                        db.Categories.Add(category);

                        try
                        {
                            db.SaveChanges();
                            result = Ok(category);
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                    else
                    {
                        result = BadRequest(ModelState);
                    }
                }
                else
                {
                    result = BadRequest("BadKey");
                }
            }
            else
            {
                result = BadRequest("BadUsername");
            }

            return result;
        }

        // DELETE: api/Categories/5
        [ResponseType(typeof(Category))]
        public IHttpActionResult DeleteCategory(string key, string username, string id)
        {
            IHttpActionResult result;

            if (db.Users.Find(username).sessionkey.Equals(key))
            {
                Category category = db.Categories.Find(id);
                db.Categories.Remove(category);
                db.SaveChanges();

                if (category == null)
                {
                    result = Ok(category);
                }
                else
                {
                    result = NotFound();
                }
            }
            else
            {
                result = BadRequest("BadKey");
            }                

            return result;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CategoryExists(int id)
        {
            return db.Categories.Count(e => e.categoryid == id) > 0;
        }
    }
}